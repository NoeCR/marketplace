export interface ProductInterface {
    id?: string;
    title?: string;
    image?: string;
    description?: string;
    price?: number;
    extras?: Array<string>;
  }
