import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { ProductInterface } from "../../models/product-interface";

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

  public products: Array<ProductInterface> = [];
  constructor(private prodService: ProductService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.prodService.getAllProducts().subscribe(
      response => {
        this.products = response;
        console.log(this.products);
      },
      err => {
        console.log(err);
      }
    );
  }
}