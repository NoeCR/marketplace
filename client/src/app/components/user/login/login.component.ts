import { Component, OnInit } from '@angular/core';
import { UserInterface } from '../../../models/user-interface';
import { NgForm } from "@angular/forms";
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title: string = 'Login';
  private user: UserInterface = {
    email: "",
    password: ""
};
  public isError: boolean = false;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
  }

  onLogin(form: NgForm) {
    console.log(form);
    if (form.valid) {
      console.log('Entra');
      return this.authService
        .loginUser(this.user.email, this.user.password)
        .subscribe(
          data => {
            console.log(data);
            this.authService.setUser(data.user);
            this.authService.setToken(data.id);
            this.router.navigate(["/marketplace"]);
            //location.reload();
            this.isError = false;
          },
          error => {
            this.onErrors();
          }
        );
    } else {
      this.onErrors();
    }
  }
  onErrors() {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
    }, 4000);
}
}
