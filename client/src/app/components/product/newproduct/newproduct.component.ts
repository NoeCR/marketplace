import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router,ActivatedRoute, Params } from '@angular/router';

import { ProductService} from '../../../services/product.service';
import { UploadService } from 'src/app/services/upload.service.js';

@Component({
  selector: 'app-newproduct',
  templateUrl: './newproduct.component.html',
  styleUrls: ['./newproduct.component.css']
})
export class NewproductComponent implements OnInit {

  public filesToUpload: Array<File>;

  constructor(private prodService: ProductService,
              private router: Router,
              private uploadService: UploadService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    const prodId = this.route.snapshot.params.id;
    this.getProduct(prodId);
  }

  onSaveProduct(prodForm: NgForm): void {
    if (!prodForm.value.id) {
      this.uploadService.uploadFile( [], this.filesToUpload, 'image' )
      .subscribe(
        response => {
          this.prodService.saveProduct(prodForm.value, response.result.files.image[0].name)
                .subscribe(prod => this.router.navigate(['marketplace']));
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.uploadService.uploadFile( [], this.filesToUpload, 'image' )
      .subscribe(
        response => {
          this.prodService.updateProduct(prodForm.value, response.result.files.image[0].name)
                .subscribe(prod => this.router.navigate(['marketplace']));
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  getProduct(id: string) {
    this.prodService.getProductById(id).subscribe(prod => (this.prodService.selectedProd = prod));
  }

  fileChangeEvent(fileInput: any) {
      this.filesToUpload = <Array<File>>fileInput.target.files;
      console.log(this.filesToUpload);
  }
}
