import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketplaceComponent } from './components/marketplace/marketplace.component';
import { LoginComponent } from './components/user/login/login.component';
import { NewproductComponent } from './components/product/newproduct/newproduct.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'marketplace', component: MarketplaceComponent },
  { path: 'product-create', component: NewproductComponent },
  { path: 'product-update/:id', component: NewproductComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
