import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';



@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(public http: HttpClient) { }

  uploadFile(params: Array<string>, files: Array<File>, name: string): Observable<any> {
    const formData: FormData = new FormData();
    for(var i=0; i < files.length; i++){
      var fileExtension = '.' + files[i].name.split('.').pop();
      var nameFile = Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;
      console.log(files[i].name);
      formData.append(name, files[i], nameFile);
      }
      return this.http.post('http://localhost:3000/api/containers/products/upload', formData);
  }
}
