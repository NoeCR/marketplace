import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { map } from "rxjs/operators";
import { isNullOrUndefined } from "util";
import { ProductInterface } from "../models/product-interface";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public products: Array<ProductInterface> = [];
  public product: Observable<any>;
  public selectedProd: ProductInterface = {
    id: null,
    title: '',
    image: '',
    description: '',
    price: 0,
    extras: []
  };

  constructor(private http: HttpClient) { }

  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });
  getAllProducts(): Observable<any>{
    const urlApi = 'http://localhost:3000/api/products';
    return this.http.get<ProductInterface>(urlApi, {headers: this.headers});
  }
  saveProduct(product: ProductInterface, image: string) {
    const urlApi = `http://localhost:3000/api/products`;
    product.image = image;
    return this.http
      .post<ProductInterface>(urlApi, product, { headers: this.headers })
      .pipe(map(data => data));
  }
  updateProduct(product: ProductInterface, image: string) {
    const urlApi = 'http://localhost:3000/api/products/';
    product.image = image;
    return this.http
      .put<ProductInterface>(urlApi + product.id, product, { headers: this.headers })
      .pipe(map(data => data));
  }
  getProductById(id: string) {
    const urlApi = `http://localhost:3000/api/products/${id}`;
    return (this.product = this.http.get(urlApi));
  }
}
